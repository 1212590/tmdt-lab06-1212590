﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SessionMVC1212590.Controllers
{
    public class UsersController : Controller
    {
        // GET: Users
        public ActionResult Index1212590()
        {
            string IsLogin = (string)Session["IsLogin"];
            if (IsLogin != "1")
            {
                return RedirectToAction("Login1212590", "Auth");
            }
            return View();
        }

        public ActionResult UserByID1212590(int mssv)
        {
            string _mssv = mssv.ToString();
            string IsLogin = (string)Session["IsLogin"];
            if (IsLogin != "1")
            {
                return RedirectToAction("Login1212590", "Auth");
            }

            switch (_mssv)
            {
                case "1212121":
                    @ViewBag.MSSV = "1212121";
                    @ViewBag.HoTen = "Hồ Đức Hiếu";
                    break;
                case "1212172":
                    @ViewBag.MSSV = "1212172";
                    @ViewBag.HoTen = "Phan Quốc Khánh";
                    break;
                case "1212433":
                    @ViewBag.MSSV = "1212433";
                    @ViewBag.HoTen = "Lê Phạm Ngọc Trâm";
                    break;
                case "1212454":
                    @ViewBag.MSSV = "1212454";
                    @ViewBag.HoTen = "Nguyễn Việt Trung";
                    break;
                case "1212481":
                    @ViewBag.MSSV = "1212481";
                    @ViewBag.HoTen = "Phan Thanh Tuấn";
                    break;
                case "1212590":
                    @ViewBag.MSSV = "1212590";
                    @ViewBag.HoTen = "Lê Khánh Sơn";
                    break;
                default:
                    @ViewBag.Error = "Không tìm thấy.";
                    break;
            }

            return View();
        }
    }
}