﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SessionMVC1212590.Models;

namespace SessionMVC1212590.Controllers
{
    public class AuthController : Controller
    {
        // GET: Auth
        public ActionResult Login1212590()
        {
            string IsLogin = (string)Session["IsLogin"];
            if (IsLogin == "1")
            {
                var returnUrl = Request.QueryString["ReturnURL"];
                if (string.IsNullOrEmpty(returnUrl))
                {
                    returnUrl = "~/";
                }
                return Redirect(returnUrl);
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        public ActionResult CheckLogin1212590(UserModel user)
        {
            if ((user.Username == "1212590") && (user.Password == "TMDT2015"))
            {
                Session["IsLogin"] = "1";
                Session["Username"] = "1212590";
                var returnUrl = Request.QueryString["ReturnURL"];
                if (string.IsNullOrEmpty(returnUrl))
                {
                    returnUrl = "~/";
                }
                return Redirect(returnUrl);
            }
            else
            {
                return RedirectToAction("Login1212590", "Auth");
            }
        }

        public ActionResult Logout1212590()
        {
            string IsLogin = (string)Session["IsLogin"];
            if (IsLogin == "1")
            {
                Session["IsLogin"] = "0";
                return RedirectToAction("Index1212590", "Home");
            }
            else
            {
                return RedirectToAction("Login1212590", "Auth");
            }
        }
    }
}